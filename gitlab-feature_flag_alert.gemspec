require_relative 'lib/gitlab/feature_flag_alert/version'

Gem::Specification.new do |spec|
  spec.name          = "gitlab-feature_flag_alert"
  spec.version       = Gitlab::FeatureFlagAlert::VERSION
  spec.authors       = ["Fabio Pitino"]
  spec.email         = ["fpitino@gitlab.com"]

  spec.summary       = %q{Tool that alerts on overdue GitLab feature flags}
  spec.description   = %q{Tool that alerts on overdue GitLab feature flags}
  spec.homepage      = "https://gitlab.com"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["allowed_push_host"] = 'http://mygemserver.com'

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com" # "TODO: Put your gem's public repo URL here."
  spec.metadata["changelog_uri"] = "https://gitlab.com"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency 'faraday'

  spec.add_development_dependency 'rspec'
end
