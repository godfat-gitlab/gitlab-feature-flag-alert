# frozen_string_literal: true

require 'yaml'

module Gitlab
  module FeatureFlagAlert
    class FeatureFlag
      ATTRIBUTES = %i[name introduced_by_url rollout_issue_url milestone type group default_enabled]

      attr_accessor *ATTRIBUTES

      def self.load_all(path)
        Dir.glob(path).map do |path|
          params = YAML.load_file(path)
          feature_flag = FeatureFlag.new(params)
        end
      end

      def initialize(params)
        ATTRIBUTES.each do |attribute|
          public_send("#{attribute}=", params[attribute.to_s])
        end
      end
    end
  end
end
