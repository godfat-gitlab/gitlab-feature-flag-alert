# frozen_string_literal: true

require 'date'

module Gitlab
  module FeatureFlagAlert
    class Issue
      DASHBOARD_URL = "https://app.periscopedata.com/app/gitlab/792066/Engineering-::-Feature-Flags".freeze
      DEFAULT_ASSIGNEE = "@m_gill".freeze

      attr_reader :group, :flags, :assignees

      def initialize(group, flags, assignees)
        @group = group
        @flags = flags
        @assignees = assignees || DEFAULT_ASSIGNEE
      end

      def title
        "Feature flags requiring attention for #{group} - #{Date.today.iso8601}"
      end

      def prepared_flags
        content = []
        flags.each do |flag|
          content << "- [ ] `#{flag.name}` - %#{flag.milestone} #{flag.rollout_issue_url ? flag.rollout_issue_url : '*Missing rollout issue'}"
        end
        content
      end

      def body
        <<~SUMMARY
          This is a group level feature flag report containing feature flags that have been enabled in the codebase for 2 or more releases.

          Please review these feature flags to determine if they are able to be removed entirely.

          Feature flag trends can be found in the [Sisense dashboard.](#{DASHBOARD_URL})

          ----

          #{prepared_flags.join("\n")}

          ----

          /label ~"#{group}"
          /assign #{assignees}
        SUMMARY
      end
    end
  end
end
